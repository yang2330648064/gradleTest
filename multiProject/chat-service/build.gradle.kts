repositories {
    mavenCentral()
}

plugins {
    `java-library`
}

dependencies {
    testImplementation(platform("org.junit:junit-bom:5.10.0"))
    testImplementation("org.junit.jupiter:junit-jupiter")
    api(project(":auth-service"))
}

tasks.test {
    useJUnitPlatform()
}