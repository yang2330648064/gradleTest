plugins {
    //id("java")
    `kotlin-dsl`
}

group = "com.yang"
version = "unspecified"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation(platform("org.junit:junit-bom:5.10.0"))
    testImplementation("org.junit.jupiter:junit-jupiter")
}

tasks.test {
    useJUnitPlatform()
}

// 声明自定义插件
gradlePlugin {
    plugins {
        create("my-custom-plugin") {
            id = "my-custom-plugin"
            implementationClass = "com.yang.MyPlugin"
        }
    }
}