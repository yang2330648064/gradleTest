package com.yang

import org.gradle.api.Plugin
import org.gradle.api.Project

class MyPlugin: Plugin<Project> {
    override fun apply(target: Project) {
        println("com.yang.MyPlugin 加载了")

        target.tasks.register("a") {
            doLast { println("hello doLast") }
        }

        target.tasks.register("b") {
            doLast { println("hello doLast") }
            dependsOn(target.tasks.named("a"))
        }

    }
}