import org.gradle.api.JavaVersion

object Version {
    val sourceVersion= JavaVersion.VERSION_22
    val targetVersion= JavaVersion.VERSION_22
    val springVersion= "6.1.3"
}