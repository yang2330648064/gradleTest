subprojects{
    apply(plugin = "java")
    group = "com.yang"
    version = "1.0"

    configure<JavaPluginExtension> {
        sourceCompatibility = Version.sourceVersion //buildSrc配置的version
        targetCompatibility = Version.targetVersion
    }

    repositories {
        mavenAlibaba()
    }
}
