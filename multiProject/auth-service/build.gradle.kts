repositories {
    mavenCentral()
}

plugins {
    `java-library`
    id("my-custom-plugin")
}

dependencies {
    testImplementation(platform("org.junit:junit-bom:5.10.0"))
    testImplementation("org.junit.jupiter:junit-jupiter")
    //implementation(project(":common-service")) //引入common模块 依赖不传递
    api(project(":common-service"))
    implementation("org.springframework:spring-beans:${Version.springVersion}")
    implementation("org.springframework:spring-aop:${Version.springVersion}")
    implementation("org.springframework:spring-web:${Version.springVersion}")
}

tasks.test {
    useJUnitPlatform()
}